# Relocate

_Hiatus. This is on hold while I focus on more foundational projects like web
components, persistent storage, and state management_

Interactive maps and a searchable directory for buildings, rooms, people, and
clubs at UVic. Designed as a progressive web application using Leaflet.js

![](./docs/images/screenshot.png)

A screencast will be uploaded when the project is closer to finished.

Move to _/packages/client/_ and either start a client development server with
`npm run start` or compile a production build with `npm run build`.

## Features

- Indexed search of rooms, people, and clubs
- Leaflet.js as the only noticable dependency
- ~~Offline and mobile ready via service workers~~ (Undone for now)
- Individual buildings could be served statically if needed

## Ingredients

This project is very close to vanilla JS. It uses webpack as a bundler. Leaflet
is the mapping library, but has been reduced to only import what's needed. The
last dependency is a hyperscript reviver; it's used to write components, think
JSX, which keeps the code much healthier than seperating HTML and JS.

## Documentation

Everything is in _docs_, though most markdown files will not be useful for
contributing and are notes made during development. Read _structure.md_ for a
breakdown of the project's tree.

## Future

This is my bike shed project where I learn new things. As such, I'll be
experimenting with web components, state management, hot reloading, metrics,
handling files, IndexedDB, and a few other things.

I like unhosted web architecture and would like to support concepts like
https://remotestorage.io/ for storage. It would be ideal if this project could
edit itself and provide a way to build maps inside the client; similar to
portable applications like TiddlyWiki.

- Support multiple buildings
- Work out data management so users can share work but also keep things private
  using remote storage or import/exports
- Allow all data structures to be accessible/debugged by devtools
- Settings panel
- Per building settings such as map tile server, canvas dimensions, polygons
- Persistent and global setting and state storage
