import { toLatLng } from 'leaflet/src/geo/LatLng'
import { LatLngBounds } from 'leaflet/src/geo/LatLngBounds'

import { Map as LeafletMap } from 'leaflet/src/map'
import { SVG } from 'leaflet/src/layer/vector/SVG'

import { Simple as SimpleCRS } from 'leaflet/src/geo/crs/CRS.Simple'
import { Transformation } from 'leaflet/src/geometry/Transformation'

import { TileLayer } from 'leaflet/src/layer/tile/TileLayer'

import { Zoom } from 'leaflet/src/control/Control.Zoom'
import { ToggleSidebar, ElevatorButtons } from './buttons'

import { Elevator } from './elevator'
import { restoreView } from './restoreView'

import 'leaflet/dist/leaflet.css'

// avoid importing Renderer.getRenderer since it pulls in 5.8kb of code from
// layer/vector/Canvas which is unused due to an if statement
const renderer = new SVG()

LeafletMap.include({
  getRenderer() {
    if (!this.hasLayer(renderer)) {
      this.addLayer(renderer)
    }
    this._renderer = renderer
    return renderer
  },
})

// the map object, set by setupMap()
let LMap = null

// convert xy image coordinates to lat/lng by xy([1, 2]) or xy(1, 2)
const xy = (x, y) => Array.isArray(x) ? toLatLng(x[1], x[0]) : toLatLng(y, x)

// original wkts were scaled by 37.788 and rounded to the nearest integer.
// producing 6656, 3328

const bounds = new LatLngBounds([xy(0, 0), xy(6656, 3328)])
const scale = 1 / 26 * 0.8125

// the url is empty because it will be set by Elevator
const MapTileLayer = new TileLayer('//:0', {
  bounds, // leafletjs.com/reference-1.2.0.html#gridlayer-bounds
  errorTileUrl: '/res/images/offline-tile.png',
  noWrap: true,
  attribution: 'UVic Facilities Management',
})

// render the map over ELW lobby
const resetView = () => {
  const defaultView = {
    center: [1536, 3400],
    level: 3,
    zoom: 2,
  }
  LMap.setView(defaultView.center, defaultView.zoom)
  Elevator.level = defaultView.level
}

const setupMap = containerNode => {
  LMap = new LeafletMap(containerNode, {
    crs: Object.assign({}, SimpleCRS, {
      transformation: new Transformation(scale, 0, scale, 0),
    }),
    maxBounds: bounds.pad(0.5), // restrict panning to image bounds
    minZoom: 2,
    maxZoom: 5,
    // zoom controls are placed manually a few lines later
    zoomControl: false,
    zoomSnap: 0.5,
  })

  // add buttons in order
  LMap.addControl(new ToggleSidebar())
  LMap.addControl(new Zoom())
  LMap.addControl(new ElevatorButtons())

  MapTileLayer.addTo(LMap)

  if (!restoreView()) resetView()
}

export { LMap, MapTileLayer, setupMap, resetView, xy }
