import { v } from 'voko'
import { Control } from 'leaflet/src/control/Control'
import { LMap } from '.'
import { Elevator } from './elevator'

// build buttons along the side of the LMap

// show/hide sidebar
const ToggleSidebar = Control.extend({
  options: {
    position: 'topleft',
  },
  onAdd() {
    return v('.leaflet-control-zoom.leaflet-bar', [
      // copy the zoom-in class which only modifies the font size
      v('a.leaflet-control-zoom-in[href=#]', {
        title: 'Toggle sidebar',
        onClick(ev) {
          ev.stopPropagation()
          const sidebar = document.querySelector('nav')
          sidebar.classList.toggle('hidden')
          // tell leaflet to recalculate layer dimensions
          LMap.invalidateSize()
        },
      }, '≡'),
    ])
  },
})

// change floor
const ElevatorButtons = Control.extend({
  options: {
    position: 'topleft',
  },
  onAdd() {
    const buttons = [
      { text: 'up', fn: this.up },
      { text: 'down', fn: this.down },
    ]
    return v('.leaflet-control-zoom.leaflet-bar', [
      buttons.map(({ text, fn }) =>
        v('a.elevator[href=#]', {
          title: `Move ${text} one floor`,
          onClick(ev) {
            ev.stopPropagation()
            fn.call(this)
            LMap.getContainer().focus()
          },
        }, [
          // TODO: icons
          v('img', {
            src: '../res/icons/arrow.svg',
            class: text === 'up' ? 'flip': '',
          }),
        ])),
    ])
  },
  up() {
    Elevator.level++
  },
  down() {
    Elevator.level--
  },
})

export { ToggleSidebar, ElevatorButtons }
