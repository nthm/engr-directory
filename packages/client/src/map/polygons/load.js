import { requestLevelPolygons } from '../../util/requests'
import { xy } from '..'
import { LayerGroup } from 'leaflet/src/layer/LayerGroup'
import { Tooltip } from 'leaflet/src/layer/Tooltip'
import { Polygon } from 'leaflet/src/layer/vector/Polygon'
import { Directory } from '../../directory'
import { styleFromDetails, shadeHex } from './style'
import { showCard, dismissCard } from '../../card'

async function loadLevelIntoLayer(level) {
  const collection = new LayerGroup()
  const polygons = await requestLevelPolygons(level)
  
  // the directory has our spaces to retrieve
  await Directory.ready
  for (const [room, line] of Object.entries(polygons)) {
    const space = Directory.collections.spaces.get(room)
    const style = styleFromDetails(space)
    // apply translation to all polygons
    let [px, py] = [172, 86]
    const polygon = new Polygon(line.map(([x, y]) => {
      px += x
      py += y
      return xy(px, py)
    }), style)
    const tooltip = new Tooltip() // see leaflet's debug/map/tooltip.html
    tooltip.setContent(room)

    const eventMap = {
      mouseover() {
        polygon.setStyle({ opacity: 0.8, weight: 4 })
        showCard(space)
      },
      mouseout() {
        polygon.setStyle(style)
        dismissCard()
      },
      click() {},
    }
    eventMap.click = () => {
      // FIXME: Leaflet bug (?) where tooltip events are removed when `.off()`
      polygon.off(eventMap)
      polygon.setStyle({
        opacity: 1,
        color: shadeHex(style.color, -0.5),
      })
      // prevent future hover events from updating the card. reset on dismiss
      showCard(space, true, () => {
        polygon
          .setStyle(style)
          .on(eventMap)
      })
    }

    polygon
      .bindTooltip(tooltip)
      .on(eventMap)

    Directory.collections.polygons.set(room, polygon)
    collection.addLayer(polygon)
  }
  return collection
}

export { loadLevelIntoLayer }
