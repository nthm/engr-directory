import { LMap } from '.'
import { Elevator } from './elevator'

// save and restore the map view state - x/y, zoom, and current level

// has the map listener been set?
let setListener = false

function restoreView() {
  let view
  if (!setListener) {
    LMap.on('moveend', () => {
      // wait until the map is loaded
      if (!LMap._loaded) {
        return
      }
      const { lat, lng } = LMap.getCenter()
      view = {
        center: [lat, lng],
        zoom: LMap.getZoom(),
        level: Elevator.level,
      }
      window.localStorage.setItem('mapView', JSON.stringify(view))
    })
    setListener = true
  } else {
    view = window.localStorage.getItem('mapView')
  }
  try {
    view = JSON.parse(view || '')
    // below `true` is to animate the transition
    LMap.setView(view.center, view.zoom, true)
    // TODO: wait until after transition to change floor?
    Elevator.level = view.level
  } catch (err) {
    return false
  }
}

export { restoreView }
