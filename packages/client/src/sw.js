const cacheName = 'directory-engr'

const urls = [
  '/',
]

self.addEventListener('install', event => {
  self.skipWaiting() // start working right away
  console.log('SW installing')
  event.waitUntil(
    caches.open(cacheName).then(cache =>
      // can't use cache.addAll(urls) because fetch needs credentials
      cache.addAll(urls.map(url =>
        new Request(url, { credentials: 'include' })))))
})

const requestThenCache = request =>
  // the Request() already has { credentials: 'include' }
  fetch(request).then(newResponse =>
    caches.open(cacheName).then(cache => {
      cache.put(request, newResponse.clone()) // don't consume the body
      return newResponse
    }))

self.addEventListener('fetch', event => {
  const { request } = event
  console.log('SW fetch:', request.url)
  // github.com/w3c/ServiceWorker/issues/1148
  event.respondWith(
    caches.match(request).then(response =>
      response || requestThenCache(request)
    ))
})
