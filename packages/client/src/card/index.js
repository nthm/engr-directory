import { v } from 'voko'
import './style.css'

// state
let timeout = null
let currentSpace = null
let live = {}

// prevent the card from being updated by any other cards
let locked = false
let unlockCallback = () => {}

// init
const Card = ({ unlockClickNode }) => {
  if (live.Card) {
    console.error('card already setup')
    return
  }
  unlockClickNode.addEventListener('click', () => {
    if (!locked) {
      return
    }
    locked = false
    startCardTimeout()
    return unlockCallback()
  })
  return v('#card', { ref: e => live.Card = e })
}

function showCard(space, lock = false, onDismiss) {
  if (locked) {
    return
  }
  if (lock) {
    // TODO: add lock icon via class
    locked = true
    if (onDismiss) unlockCallback = onDismiss
  }
  if (currentSpace === space) {
    return
  }
  currentSpace = space
  clearTimeout(timeout)
  const { name, room, desc, tags, dept, type, area, staff, updated } = space

  live.Card.innerHTML = ''
  live.Card.appendChild(
    v.fragment(
      v('h2', name || room),
      name && v('h3', room),
      v('p', desc || v('em', 'No description')),
      tags && v('p', 'Tags: ', tags.split(' ').map(t => v('span.tag', t))),
      v('p', 'Department: ', dept || v('em', 'Not set')),
      v('p', 'Space type: ', type || v('em', 'Not set')),
      v('p', `Area: ${area || v('em', '?')}m`, v('sup', 2)),
      staff &&
        v('', [
          v('p', `Staff: ${staff.length}`),
          staff.map(person =>
            v('.staff', [
              v('p', person.name),
              v('p', person.contact || v('em', 'No contact')),
            ])),
        ]),
      v('small', 'Last modified: ', updated || 'Never')))
  live.Card.className = 'active'
}

function startCardTimeout() {
  if (locked) {
    return
  }
  timeout = setTimeout(() => {
    live.Card.className = ''
    currentSpace = null
  }, 1000)
}

export { Card, showCard, startCardTimeout as dismissCard }
