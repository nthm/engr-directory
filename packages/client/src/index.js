import { v } from 'voko'
import { Sidebar } from './sidebar'
import { Modal } from './modal'
import { Card } from './card'

import { setupMap, resetView } from './map'
import { setupDirectory } from './directory'

import './style.css'

const live = {
  Map: v('#map'),
}

document.querySelector('#root').appendChild(
  v.fragment([
    v(Sidebar),
    v(Modal),
    v('main', [
      live.Map,
      v(Card, {
        // element where clicks will unlock a locked card
        unlockClickNode: live.Map,
      }),
    ]),
  ]))

// force the browser to render before continuing. sets dimensions for #map
window.getComputedStyle(live.Map)
setupMap(live.Map)
setupDirectory()

const https = window.location.protocol === 'https'
if (https && 'serviceWorker' in window.navigator) {
  window.navigator.serviceWorker.register('/sw.js')
    .then(reg => {
      console.log('sw registered', reg)
    })
}

if (window.location.hash === '#kiosk') {
  const idleTimeout = 1000 * 60 * 5 // 5 minutes
  let time
  document.addEventListener('mousemove', () => {
    clearTimeout(time)
    time = setTimeout(() => {
      console.log('resetting application due to inactivity')
      // TODO: restore search state too
      resetView()
    }, idleTimeout)
  })
}
