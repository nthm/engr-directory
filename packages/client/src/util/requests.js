const req = url =>
  fetch(url, { credentials: 'include' })
    .then(res => {
      if (!res.ok) throw res.status
      return res.json()
    })

// TODO: derive the building from the URL or menu...
const requestDirectoryPayloads = () => {
  const result = {}
  const requests =
    ['spaces', 'info', 'staff'].map(name =>
      req(`/data/engineering/details/${name}.json`).then(data => {
        result[name] = data
      }))

  return Promise.all(requests).then(() => result)
}

const requestLevelPolygons = level =>
  req(`/data/engineering/polygons/${level}.json`)

export { requestDirectoryPayloads, requestLevelPolygons }
