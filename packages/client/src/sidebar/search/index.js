import { v } from 'voko'
import { Directory, stats } from '../../directory'
import { centerMapOnSpace } from '../../map/toSpace'
import { showCard } from '../../card'

// used for performing search
let displayLimit = 10
let results = new Set()
let savedQuery = ''

let hot = false
let wantsToDraw = false

const live = {}

const Search = ({ inputBox, ...attr }) => {
  inputBox.addEventListener('input', event => {
    const value = event.target.value.trim()
    if (value === '') {
      live.Help.className = ''
      live.Results.className = 'hidden'
      savedQuery = ''
      return
    }
    if (savedQuery === value) {
      return
    }
    savedQuery = value
    results = Directory.search(value) // is a Set()
    draw()
  })

  // add to the help-message swap a little
  Directory.ready
    .then(() => {
      live.Help.appendChild(
        v.fragment([
          v('p', `${stats.spaces} spaces, ${stats.info} spaces, and ${stats.staff} staff`),
          v('p', `Indexed ${Directory.index.size} tokens in ${stats.indexTime}`),
        ]))
    })

  return v('', attr, [
    v('#help', {
      ref: e => live.Help = e,
    }, [
      v('h1', 'Type to search'),
      v('p', 'For rooms, tags, people, clubs, descriptions, contact info...'),
      // index stats once Directory.ready resolves
    ]),
    v('.hidden', {
      ref: e => live.Results = e,
    }),
  ])
}

// similar to the hover card but less information
const ListItem = ({ name, room, desc, tags, dept, staff }) =>
  v('article.list-item.search', {
    onClick() {
      // TODO: just click the room. that will lock the card and darken it too
      centerMapOnSpace(room)
      showCard({ name, room, desc, tags, dept, staff })
    },
  }, [
    v('h3', name || room),
    name && v('h4', room),
    desc && v('p', desc),
    tags && tags.split(' ').map(t => v('span.tag', t)),
    dept && v('p', dept),
    staff && v('', [
      v('p', `Staff: ${staff.length}`),
      staff.map(person => v('p.staff', person.name)),
    ]),
  ])

// this is its own function because it calls itself
function draw() {
  // if display is still cooling down, leave
  if (hot) {
    console.log('...still cooling down')
    wantsToDraw = true
    return
  }
  console.log('drawing (no redraws for 100ms)')
  live.Help.className = 'hidden'
  live.Results.className = ''

  live.Results.innerHTML = ''
  live.Results.appendChild(
    v('', { style: 'padding: 0 20px 10px' }, [
      v('p', `${results.size} result${results.size !== 1 ? 's' : ''}`),
      v('small', stats.searchTime),
    ])
  )

  let count = 0
  for (const entry of results) {
    if (count++ === displayLimit) break
    live.Results.appendChild(v(ListItem, entry))
  }

  hot = true
  wantsToDraw = false
  setTimeout(() => {
    hot = false
    if (wantsToDraw) draw()
  }, 200)
}

export { Search }
