import { v } from 'voko'
import { Categories } from './categories'
import { Search } from './search'
import { Keyboard } from './keyboard'

import './style.css'

// the SVG is used in a ::before element via CSS, not directly here
import '../../res/icons/search.svg'

// some components are defined outside of the sidebar when they need to
// reference live DOM nodes for event listeners

const hide = e => e && e.classList.add('hidden')
const show = e => e && e.classList.remove('hidden')

const live = {}

const Sidebar = () =>
  v('nav', [
    // yes this needs to be seperate and not `nav.scrollable`
    v('.scrollable', [
      v('header', [
        v('img', {
          src: '../res/images/uvic-logo.png',
          alt: 'University of Victoria',
        }),
        v('', 'Engineering Directory'),
      ]),
      v('#search', [
        v('input[placeholder=Search]', {
          ref: e => live.SearchInput = e,
          onFocus() {
            hide(live.CategoryBox)
            show(live.SearchBox)
            show(live.ShowCategories)
            show(live.Keyboard)
          },
        }),
        v('.hidden', {
          ref: e => live.ShowCategories = e,
          onClick() {
            show(live.CategoryBox)
            hide(live.SearchBox)
            hide(live.ShowCategories)
            hide(live.Keyboard)
          },
        }, 'Show categories'),
      ]),
      v(Categories, {
        ref: e => live.CategoryBox = e,
      }),
      v(Search, {
        ref: e => live.SearchBox = e,
        inputBox: live.SearchInput,
        class: 'hidden',
      }),
    ]),
    // FIXME: should be === not !== (but testing for now)
    window.location.hash !== '#kiosk' &&
      v(Keyboard, {
        ref: e => live.Keyboard = e,
        inputBox: live.SearchInput,
      }),
  ])

export { Sidebar }
