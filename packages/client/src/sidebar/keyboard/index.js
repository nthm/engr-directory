import { v } from 'voko'
import './style.css'

const rows = ['qwertyuiop', 'asdfghjkl', 'zxcvbnm', ' ']
let uppercase = false

const Keyboard = ({ inputBox, ...attr }) => {
  const keyboardElement =
    v('.keyboard.hidden', attr, rows.map(row => {
      const rowElement =
        v('', row.split('').map(char =>
          v('kbd', {
            style: row !== ' ' ? '' : 'min-width: 60%',
            onClick() {
              inputBox.value += uppercase ? char.toUpperCase() : char
              inputBox.dispatchEvent(new Event('input'))
            },
          }, char)))
      if (row.startsWith('z')) {
        const shiftKey =
          v('kbd', {
            style: 'flex-grow: 1',
            onClick() {
              keyboardElement.classList.toggle('uppercase')
              uppercase = !uppercase
            },
          }, '^')
        const backspaceKey =
          v('kbd', {
            style: 'flex-grow: 1',
            onClick() {
              const string = inputBox.value
              inputBox.value = string.substring(0, string.length - 1)
              inputBox.dispatchEvent(new Event('input'))
            },
          }, '~')
        rowElement.insertBefore(shiftKey, rowElement.firstElementChild)
        rowElement.appendChild(backspaceKey)
      }
      return rowElement
    }))
  return keyboardElement
}

export { Keyboard }
