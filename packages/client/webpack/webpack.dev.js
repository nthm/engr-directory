const webpack = require('webpack')
const config = require('./webpack.common')

config.mode = 'development'

// adds /* filename */ comments to generated require()s
config.output.pathinfo = true
config.optimization = {
  namedModules: true,
  concatenateModules: false,
}

config.plugins.push(
  new webpack.HotModuleReplacementPlugin())

module.exports = config
