const express = require('express')
const path = require('path')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')

const addBuildingRoutes = require('../../building-data/addExpressRoutes')

const app = express()
const config = require('./webpack.dev')
const compiler = webpack(config)

app.use(webpackDevMiddleware(compiler, { noInfo: true }))
app.use(webpackHotMiddleware(compiler))

const nonWebpackFiles = path.join(__dirname, '..', 'res')
app.use('/res', express.static(nonWebpackFiles))

// inject routes for finding building data
addBuildingRoutes(app, express)

app.listen(3000, () => {
  console.log('Listening on 3000\n')
})
