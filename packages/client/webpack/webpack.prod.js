const TerserPlugin = require('terser-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const config = require('./webpack.common')

config.mode = 'production'

config.optimization = {
  concatenateModules: false, // if true then all the bundle analysis is useless
  minimizer: [
    new TerserPlugin({
      cache: true,
      parallel: true,
      extractComments: true,
    }),
    new OptimizeCSSAssetsPlugin({}),
  ],
}

config.plugins.push(
  new BundleAnalyzerPlugin({
    analyzerMode: 'static',
    openAnalyzer: false,
    generateStatsFile: false,
  }))

module.exports = config
