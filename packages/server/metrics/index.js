/**
 * Metrics
 * 
 * Store bandwidth of requests for resources. Have an API that the client can
 * ping to discuss use and amount of cached resources. Maintain a history of
 * actions per session allowing to see the flow throughout the app.
 * 
 */
