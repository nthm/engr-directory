const express = require('express')
const fs = require('fs')
const path = require('path')
const morgan = require('morgan')
const bodyParser = require('body-parser')

const passport = require('passport')
const mongoose = require('mongoose')
const session = require('express-session')
const MongoSessionStore = require('connect-mongo')(session)
const dotenv = require('dotenv')
dotenv.config()

// XXX: Likely remove for prod
process.on('unhandledRejection', (reason, promise) => {
  console.error('Uncaught error in', promise)
  console.error(reason)
  process.exit(1)
})

const setupPassport = require('./auth/setup')
const addBuildingRoutes = require('../building-data/addExpressRoutes')

const logpath = path.join(__dirname, 'access.log')
const logstream = fs.createWriteStream(logpath, { flags: 'a' })

const app = express()
app.disable('x-powered-by')

mongoose.connect(process.env.DB_URI, { useNewUrlParser: true })
  .then(() => console.log('Database connected'))

app.use(session({
  secret: process.env.SESSION,
  saveUninitialized: false,
  resave: false,
  store: new MongoSessionStore({ mongooseConnection: mongoose.connection }),
}))
app.use(morgan('dev'))
app.use(morgan('common', {
  stream: logstream,
  skip: (req, res) =>
    req.path.startsWith('/data')
    && (res.statusCode == 200 || res.statusCode == 304),
}))
app.use(bodyParser.json())

app.use(passport.initialize())
app.use(passport.session())
setupPassport(passport)

// Metrics
// app.use(metrics)
// app.use('/metrics', metricsRouter)

const clientDir = path.join(__dirname, '..', 'client')
const compiledWebpackFiles = path.join(clientDir, 'build')
const nonWebpackFiles = path.join(clientDir, 'res')

// Routes for the application base
app.use('/', express.static(compiledWebpackFiles))
app.use('/res', express.static(nonWebpackFiles))

// Routes for building data
addBuildingRoutes(app, express)

const ok = (req, res) => res.sendStatus(200)
app.use('/auth/cas', passport.authenticate('cas'), ok)
app.use('/auth/dev', passport.authenticate('dev'), ok)
app.use('/auth/kerberos', passport.authenticate('kerberos'), ok)

// app.post('/update/:what(info|space|staff)/:id')

app.listen(process.env.PORT || 3000, () => {
  console.log((new Date()).toLocaleString())
  console.log(`Listening on ${process.env.PORT}\n`)
})
