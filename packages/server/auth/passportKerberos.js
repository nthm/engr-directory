// Updated passport-negotiate (from 2015) to latest Kerberos release v1.1.2:
// https://github.com/mongodb-js/kerberos/tree/v1.1.2

const AbstractStrategy = require('passport-strategy')
const util = require('util')
const kerberos = require('kerberos')

function Strategy(options, verify) {
  AbstractStrategy.call(this)
  if (!options.idpURL) {
    throw 'Kerberos Passport: Must provide a Kerberos IdP via `idpURL` option'
  }
  if (!verify || typeof verify !== 'function') {
    throw 'Kerberos Passport: Must provide a `verify` callback'
  }
  this._verify = verify
  this.name = 'kerberos-negotiate'
  this.service = `HTTP@${options.idpURL}`
}

util.inherits(Strategy, AbstractStrategy)

Strategy.prototype.authenticate = function (req) {
  const auth = req.get('Authorization')
  const self = this
  if (!auth) {
    // Generates a 401 and `WWW-Authenticate: Negotiate` HTTP header
    return self.fail('Negotiate')
  }
  if (auth.lastIndexOf('Negotiate ', 0) !== 0) {
    self.error(`Malformed negotiation auth token: ${auth}`)
    return
  }
  const challenge = auth.substring('Negotiate '.length)
  kerberos.initializeServer(this.service) // ie. 'HTTP@uvic.ca'
    .then(kerberosServer => {
      console.log('Initialized Kerberos server for', this.service)
      console.log('Pre-auth username:', kerberosServer.username)
      kerberosServer.step(challenge)
        .then(() => {
          // Response is updated in the context without a return value
          const { response, username, contextComplete } = kerberosServer
          console.log('Response:', response)
          console.log('Post-auth username:', username)
          if (!contextComplete) {
            console.log('Post-auth but contextComplete is false')
          }
          // Store result
          const principal = username
          if (req.session) {
            req.session.authenticatedPrincipal = principal
          }
          req.authenticatedPrincipal = principal

          function doneCallback(err, user, info) {
            if (err) {
              return self.error(err)
            }
            if (!user) {
              return self.fail(info)
            }
            self.success(user, info)
          }

          // Call the given verify function to let the app have final say
          return this._verify(principal, doneCallback)
        })
    })
    .catch(err => {
      self.error('Kerberos error: ' + err)
    })
}

module.exports = { Strategy }
