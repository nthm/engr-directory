const StrategyCAS = require('./passportCAS').Strategy
const StrategyDev = require('./passportDev').Strategy
const StrategyKerberos = require('./passportKerberos').Strategy

// Passport takes an identifier, usually a username, and then calls a verify
// function giving the app a chance to link the identity to a profile or decide
// to deny the authentication.

// Currently there are no profiles and any valid identifier is accepted. Note:
// Permissions will likely be looked up during the verify() stage.

// TODO: Define a `profile`. Determine what is returned from CAS.

module.exports = function setupPassport(passport) {
  passport.serializeUser((user, done) => done(null, user))
  passport.deserializeUser((user, done) => done(null, user))

  passport.use(new StrategyCAS({
    idpURL: 'https://uvic.ca/cas',
    spURL: 'https://relocate.uvsd.ca',
  }, (login, done) => {
    // User.findOne({ id: login }).then(...)
    return done(null, { profile: login })
  }))
  
  passport.use(new StrategyKerberos({
    idpURL: 'uvic.ca',
    // Don't need to provide spURL because it's in my /etc/keytab
  }, (netlink, done) => {
    // User.findOne({ id: netlink }).then(...)
    return done(null, { profile: netlink })
  }))

  // Hope no one has the Netlink 'dev'
  passport.use(new StrategyDev({
    username: 'dev',
    key: process.env.DEV_KEY,
  }))
}
