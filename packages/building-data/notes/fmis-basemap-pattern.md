I originally tried this almost a year ago and decided it wasn't worth it/wasn't
possible because the maps were too human made.

- The coordinate system is very arbitrary and human made
- Floors are always laid out in a grid, but there's no guarantee that it will
  look that way without architecture graphics enabled. The SUB is a great
  example of that
- Architecture isn't part of the the given bounding box for a floor's polygons.
  Currently I'm assuming it won't cross over into another floors's "region" but
  hard to say
- The padding around a floor is... inconsistent. Sometimes "Zoom to Extents",
  which uses the bounding box of the floor, will have a floor pushed right up
  against the border of the image. Other times there's enough whitespace for
  another floor entirely
- Some buildings, like Stadium and the University Centre, have 2 or 3 real
  floors at diagonals of a 2x2 grid but another architectural-graphics-only
  floor in the other spots (!) Maybe this can be skipped because it's rare
- There's a collection of "halls" that when you open it up you know something's
  weird because the padding between floors is 5x the actual floor... It's
  because other buildings (other ones named "halls") go in that space. Note that
  extent zooming gets the graphics in view OK because the bounding box is
  provided
- Not all buildings have a graphic for their roof. It's very uncommon. They'll
  need to be added manually or ignored
- Sometimes the padding is just wrong... They set the bounding box super high
  for seemingly no reason and that's just the way it is.

I think it's safe to assume that using the largest of all the floors as a
bounding box to derive coordinates for all other ones is fine. An exception to
that is if one floor in the middle of 3 is smaller, then the largest bounding
box won't fit.

Drawing a bunch of boxes helps you to think about it:

Imagine there are 7 floors. We know:
  - The bounding box of the entire floor which **includes** architecture
  - Floors are in a grid
  - The coordinates of the centroids and bounding boxes (not including
    architecture) of all floors are given
  - It's possible to find the largest floor via the above
  - It's likely possible to fit that largest bounding box in a grid to overlay
    the map

This is really really close... The best approach, however, would be to use some
image processing to know the boundary when all architectural drawings stop.

Seems to always be true: There's a gap between the grid of every building. So
the 2 gaps in a 3 across and 2 down grid will have no pixels other than white
down them. I can use that.

For Phantom.js I have the map working via injecting JS to the console:

```
map.moveTo(new OpenLayers.LonLat(160,20))
```

Also, I'll parse the data for the chunked polygons in the browser. Turning the
HTML into DOM so I can extract the data to JSON (or similar)
