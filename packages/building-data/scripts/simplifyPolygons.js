const fs = require('fs')
const path = require('path')

const baseDir = path.resolve(__dirname, '..', 'res')
const existingDir = path.join(baseDir, 'polygons')
const newDir = path.join(baseDir, 'new-polygons')

const chars = line => JSON.stringify(line).length

if (!fs.existsSync(newDir)) {
  fs.mkdirSync(newDir)
}

let totalDelta = 0

const files = fs.readdirSync(existingDir)
files.forEach(filename => {
  const filepath = path.join(existingDir, filename)
  console.log(filename)
  let fileDelta = 0

  const data = require(filepath)
  const newData = {}
  Object.entries(data).forEach(([room, line]) => {
    const newLine = makeRelative(simplifyLine(line))
    newData[room] = newLine

    const listDelta = line.length - newLine.length
    const roomDelta = chars(newLine) - chars(line)
    fileDelta += roomDelta

    let log = room.padEnd(10) + (roomDelta + ' bytes. ').padStart(12)
    if (roomDelta > 0) {
      log += 'WORSE'
    }
    if (roomDelta === 0) {
      log += 'NO CHANGE'
    }
    if (listDelta !== 0) {
      log += `Redundant points removed: ${listDelta}`
    }
    console.log(log)
  })
  totalDelta += fileDelta
  console.log(`Delta for file: ${fileDelta} bytes`)
  console.log()

  const newDataString =
  Object.entries(newData)
    .map(([room, line]) => `  "${room}": ${JSON.stringify(line)}`)
    .join(',\n')

  const savePath = path.join(newDir, filename)
  fs.writeFile(savePath, '{\n' + newDataString + '\n}', err => {
    if (err) throw err
  })
})

console.log(`Total delta: ${totalDelta} bytes`)

function simplifyLine(line) {
  const newLine = []
  newLine.push(line[0], line[1])
  for (let i = 2; i < line.length; i++) {
    const [[ppx, ppy], [px, py]] = newLine.slice(-2)
    const lastSlope = (ppy - py) / (ppx - px)

    const [x, y] = line[i]
    const currentSlope = (py - y) / (px - x)
    if (lastSlope === currentSlope) {
      newLine.pop()
    }
    newLine.push([x, y])
  }
  return newLine
}

function makeRelative(line) {
  return line.map(([x, y], index) => {
    if (index === 0) return [x, y]
    const [px, py] = line[index - 1]
    return [x - px, y - py]
  })
}
