import json, os
from lxml import html
from lxml.etree import fromstring, XMLParser

# split the fmis floor output into a csv of wkt geometries and a json of their
# data fields (area, department, category)

# the wkts need to be scaled by 37.788 and then moved into place on the map

def parse_file(file_path):
    # can't use a comma for the csv because wkts use them
    geometries = ['ROOM\tWKT']
    details = []

    with open(file_path) as data:
        lines = data.readlines()

    # pullfmis.py uses newlines as gaps to seperate groups of 10
    gap_indices = []
    for index, line in enumerate(lines):
        if line == '\n':
            gap_indices.append(index)
    # files don't end with a newline. so fake one
    gap_indices.append(None)

    # for part slicing from 0
    prev = 0
    parser = XMLParser(recover=True)
    for gap in gap_indices:
        xml = fromstring(''.join(lines[prev: gap]), parser=parser)
        prev = gap

        geo_elems = xml.xpath('geometry')
        for geo in geo_elems:
            geometries.append(geo.attrib['object_code'] + '\t' + geo.text)

        details_html = html.fromstring(xml.xpath('results').pop().text)
        span_elems = details_html.xpath('span')
        for span in span_elems:
            divs = span.xpath('table/tr/td/div[@class="ResultsFieldValue"]')
            # room, building, room, sub category, area, category, department
            # skip the first two and rewrite the keys
            keys = ['room', 'type', 'area', 'code', 'dept']
            values = [value.text for value in divs[2:]]

            details.append(json.dumps(dict(zip(keys, values))))

    with open(file_path + '.wkt.csv', 'w') as csv_out:
        for line in geometries:
            csv_out.write(line + '\n')

    with open(file_path + '.json', 'w') as details_out:
        for json_object in details:
            details_out.write(json_object + '\n')

# start of script

print('saves .wkt.csv and .json to the directory the input file is in')
location = input('relative root of fmis responses?: ')

for directory, subdirs, files in os.walk(location):
    print('directory:', directory)
    for floor in files:
        path = os.path.join(directory, floor)
        print(path)
        parse_file(path)

