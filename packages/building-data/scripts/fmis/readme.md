# fmis.uvic.ca

wanted to automate the way images and polygons could be pulled from the fmis
server. ended up being slightly too ambitious as reverse engineering the
system took more time than expected

the script was supposed to do three things:

- ask for the building and floors to pull data from, and if the data should
include images, polygons, or space details (properties)

- move to that location, and extract the data

- parse the data if necessary and sort it into directories

## user input

this was initially easy, and there's code to extract the sessions, buildings,
and floors from the html of the page. however, once requests were sent to
`url_mapagent` (see script) exceptions were thrown server side; usually 522

the script follows the browser's network requests as closely as possible, and
lots of time has went into debugging each one (think url encoding and required
parameters). you could take the prepared url that fails in the script, paste
it into a browser, and it'll work. then resending via the script would work,
as if the browser fixed it

ended up using a browser's existing session to get the script to work. this
should be fixed in the future to make everything easier to use, but will do
the job for now

## images

after a lot of research, it didn't seem possible to extract the images from
the server. when loading a floor, the coordinates and bounding box of the
floor's polygons are given - but the architecture's bounds are not. the map
is also very...*human* made: the origin is arbitrary, and the architecture of
each floor sometimes overlaps with its neighbour

all attempts at creating an algorithm to add padding to the bounds of a floor
are at best a guess, and would not hold true for all buildings on campus

in the end, the images would still need to be processed in gimp (or similar)
to be fixed up and merged. image extraction was removed to keep the script
simple. notes will be uploaded as a snippet to the repo

## polygons and details

polygons are handled through mapguide/mapagent but the details are not. as
you can see in a browser session, only the last ten spaces have their details
returned. the script requests all space ids from mapagent, and then batches
requests for details in groups of ten. the WKT strings from FMIS are parsed into
a format that can directly used by Leaflet's `L.Polygon`.

## parsing

this ended up being its own file, `parsefmis.py`, because the directory tree
has already been populated by `pullfmis.py` when it needed to be written

consider merging it to `pullfmis.py` as a function for completeness

## instructions

see `howto.md` for instructions on how to run the scripts until they can be
self sustaining (if there's ever time)
