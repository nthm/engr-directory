import requests
from sys import exit
from re import search
from lxml import html
from lxml.etree import fromstring, tostring
from pprint import pprint
from time import sleep
from pathlib import Path

# the base url will try to redirect to /famis/vm.php using window.location()
url           = 'https://fmis.uvic.ca'
url_vm        = url + '/famis/vm.php'
url_vmutil    = url + '/famis/vmUtil.php'
url_vmresults = url + '/famis/vmResults.php'
url_mapagent  = url + '/mapguide/mapagent/mapagent.fcgi'

# don't appear as 'User-Agent: python-requests'
headers = {
    'Host': 'fmis.uvic.ca',
    'Referer': 'https://fmis.uvic.ca/famis/vm.php',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0'
}
s = requests.Session()
s.headers.update(headers)

# a fmis session only last 10 minutes unless the browser is open
# browsers send a POST request "action: SET_VP_DISPLAY_COOKIE" to renew it

# the MapGuide and PHP sessions should be in sync
# send the wrong parameter, and it'll persist - breaking the session
session   = ''
phpsessid = ''
bcode     = ''
proxy     = 'socks5://localhost:1080'
save_to   = 'fmis'

socks = dict(http=proxy, https=proxy) if proxy else None
history = []

def req(url, query={}):
    global s, history
    # request is prepared to show url BEFORE, else it might break our session
    url = requests.Request('GET', url, params=query).prepare().url
    print(url)
#   if ask('modify query?'):
#       url = input('paste prepared query: ')

    page = s.get(url, proxies=socks)
    history.append(page)

    if page.ok is False or page.content.startswith(b'Could not get map'):
        print('not ok. crashing. reason:', page.reason)
        print(page.content[:80])
        exit(0)

    return page.content

# load the famis client, setting the session and cookies
# famis loads the first site, building, and floor for a new session
# there's only one site for UVic
def load_client():
    content = req(url_vm)
    return html.document_fromstring(content)

def extract_session(client):
    script_text = client.head.xpath('script[not(@src)]/text()').pop()
    return search('sessionId = \"(.+)\"', script_text).group(1)

def display_buildings(client):
    select = client.body.xpath("//select[@id='MapID']").pop()
    for option in select.getchildren():
        print('value: {}\t{}'.format(option.attrib['value'], option.text))

def ask(msg):
    return input(msg + ' y/N ').lower().startswith('y')

# useful for debugging in the repl
def replay(index):
    req(history[index].url)

# fmis only returns details of the last 10 spaces
# we want all of them. so batch requests in groups of 10 and merge after

# reduces a large xml of space ids into a list of xmls with ten ids each
"""
  <FeatureInformation>
  <FeatureSet ...>
    <Layer ...>
      <Class ...>
        <ID> (many)
  <Tooltip/>
  <Hyperlink/>
  <Property> (many)
"""
def reduce_space_ids_xml(xml):
    # FeatureSet comes with unwanted attributes, so take the Layer instead
    xml = xml.xpath('//Layer').pop()

    # want to clean the xml. extract the IDs as that happens:
    ids = []
    for e in xml.iter('*'):
        if e.text is not None: e.text = e.text.strip()
        if e.tail is not None: e.tail = e.tail.strip() + '\n'
        if e.tag == 'ID':
            e.getparent().remove(e)
            ids.append(e)
    pprint(ids)

    # the xml now has no IDs
    # build the missing FeatureSet. note: could just attrib.clear()?
    xml_frame = b'<FeatureSet>' + tostring(xml) + b'</FeatureSet>'
    pprint(xml_frame)

    id_groups = [ids[i:i + 10] for i in range(0, len(ids), 10)]
    reduced_xmls = []
    for group in id_groups:
        xml = fromstring(xml_frame)
        ids_parent = xml.xpath('//Class').pop()
        for id_elem in group:
            ids_parent.append(id_elem)
        reduced_xmls.append(xml)
    return reduced_xmls

# start of the script:

# option to navigate to the right building in a web browser and ride on the
# existing session from here
if not session and ask('have a MapGuide session?'):
    session = input('session: ')

if not phpsessid and ask('have a PHP session?'):
    phpsessid = input('PHP session (cookie): ')

if not bcode and ask('know the building code?'):
    bcode = input('building: ')

if not all([session, phpsessid, bcode]):
    # ok we have to load a client
    client = load_client()
    if not session:
        session = extract_session(client)
    if not bcode:
        # building codes aren't the shown in fmis, they're hidden in the html
        display_buildings(client)
        bcode = input('select a building value: ')
else:
    print('not loading client. reading from existing browser session\n')
    s.cookies.update({
        'PHPSESSID': phpsessid,
        'vpdisplay': 'N' # not sure if this is required
    })

print('MapGuide session:', session)
print('PHP session:', s.cookies['PHPSESSID'] + '\n')

# absolute base save path
base_path = Path(__file__).resolve().with_name(save_to)

print('switching to the same building breaks the session!')
if ask('switch to the building?'):
    content = req(url_vmutil, {
        'action': 'CREATE_MAP',
      # 'seq': random(), # not sure why browsers have this
        'mapid': bcode
    })

    # the response has:
    """
      <legend_html>  an xml + html document for the ui to <layers>
      <layers>       available map tile layers (themes, graphics, colours)
      <new_map/>     has map_id and {min,max}{x,y} dimensions for the map
      <floor_list>   an xml + html document with <option> tags of each floor
    """
    xml = fromstring(content)
    select_html = xml.xpath('//floor_list').pop().text
    options = html.document_fromstring(select_html).xpath('//option')

    # skip the first item in the dropdown: ALL FLOORS
    floors = [(option.attrib['value'], option.text) for option in options[1:]]
    pprint(floors)
else:
    floor_codes = input('enter codes seperated by spaces: ').split(' ')
    floors = [(floor, str(index + 1)) for index, floor in enumerate(floor_codes)]

building_path = base_path / bcode
print('save directory for this building:', building_path)

# iterate over the floors in that building
for floor_code, floor_name in floors:
    print('\non floor', floor_name)
    if not ask('visit?'):
        continue

    floor_path = building_path / floor_name
    print('save directory for this floor:', floor_path)
    floor_path.mkdir(parents=True, exist_ok=True)

    print('loading floor')
    content = req(url_vmutil, {
        'action': 'CREATE_SELECTION_XML',
        'layer': 'FLOOR',
        'id': floor_code,
    })
    # the response has:
    """
      <selection>    an xml document of <ID> tags for each floor.(not useful)
      <extents/>     with attributes for the floor size
    """
    extents = fromstring(content).xpath('extents').pop().attrib
 
    # load polygons for the floor
    wkt_order = [
        ['llx', 'lly'],
        ['urx', 'lly'],
        ['urx', 'ury'],
        ['llx', 'ury'],
        ['llx', 'lly']
    ]
    poly = ','.join([' '.join([extents[a] for a in xy]) for xy in wkt_order])
    polygon = 'POLYGON((' + poly + '))'

    # load all space ids within those boundaries
    # these requests were uppercase in the browser so the style is adopted
    load_space_ids = {
        'OPERATION': 'QUERYMAPFEATURES',
        'VERSION': '1.0.0',
        'SELECTIONVARIANT': 'INTERSECTS',
        'PERSIST': '0',
        'MAPNAME': 'FAMISMap',
      # spent two weeks debugging this comma between the values of LAYERNAMES
      # not being url encoded. field is not required though
      # 'LAYERNAMES': 'Theme_0%2CSpace',
        'MAXFEATURES': '-1',
        'LAYERATTRIBUTEFILTER': '3',
        'CLIENTAGENT': 'FAMIS Open Layers',
        'GEOMETRY': polygon,
        'SESSION': session
    }
    pprint(load_space_ids)

    # requests encodes ( ) and changes ' ' to '+' instead of %20
    # both cause famis to raise an exception
    # stackoverflow.com/q/23496750/prevent-requests-percent-encoding-urls
    query = '&'.join('%s=%s' % (k,v) for k,v in load_space_ids.items())
    encoded_query = requests.utils.quote(query, safe='=,&()')

    content = req(url_mapagent, encoded_query)

    entire_space_xml = fromstring(content)
    # returns a list of xml objects each with 10 space IDs
    small_space_xmls = reduce_space_ids_xml(entire_space_xml)

    responses = []
    for xml in small_space_xmls:
        nice_xml = tostring(xml, encoding='UTF-8', xml_declaration=True)
        print('requesting 10 IDs')
        # browsers send the xml twice, in selectionxml and imagexmltext
        content = req(url_vmresults, {
            'selectiontype': 'VECTOR',
            'mapname': 'FAMISMap',
            'imagexmltext': nice_xml,
            'selectionxml': nice_xml
        })
        responses.append(content)
        pprint(content)
        # don't spam the server... although the browsers seem to
        sleep(1)

    with open(floor_path / floor_code, 'wb') as save_file:
        save_file.write(b'\n'.join(responses))

    print('saved')
    print('finished floor:', floor_name)

print('bye')
