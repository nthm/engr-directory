# how to pull from fmis.uvic.ca

because of issues listed in `readme.md`, you have to know the codes for the
building and its floors to avoid loading a client with a new session. run the
script until it provides them, and ^C right after, since continuing will throw
an exception server side

if you're connecting from outside campus, you'll need a tunnel to access the
internal server. `ssh -fN -D 1080 [redacted internal server]` will create a
socks5 tunnel availiable on localhost:1080. set your browser's proxy and the
script `proxy` variable as needed

acquiring a valid session is messy. you need to open a browser to
fmis.uvic.ca, load it to the building, and open the developer tools:

- catch the mapguide session. it's availiable as a field in the inline script
in the head element, and attached as a GET parameter to every /mapagent
request (such as loading map images)

- catch the php session id - PHPSESSID - which is in your cookies

either open the file `pullfmis.py` and put those fields in for `session` and
`phpsessid`, respectively; or, run it and you'll be asked to paste them

note if `python3 pullfmis.py` doesn't work you likely need the lxml and
requests modules. use pip, pipenv, or your distribution's package manager

when asked, enter the building and floor codes you wrote down earlier

it should say that it's *not* loading a client. that's good. it will mention
that switching buildings is bad and breaks the session - don't do it

follow the prompts, as it will ask you to visit each floor
