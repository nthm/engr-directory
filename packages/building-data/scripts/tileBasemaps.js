const sharp = require('sharp')
const path = require('path')

const basemapDir = path.resolve(__dirname, 'basemaps')

// TODO: Just count the number of basemaps
const FLOORS = [0, 1, 2, 3, 4, 5, 6, 7]
const TILE_SIZE = 256

async function loop() {
  for (const floor of FLOORS) {
    const imagePath = path.join(basemapDir, `${floor}.png`)
    const image = sharp(imagePath)
    const { width, height } = await image.metadata()
    console.log(`Image ${imagePath} is ${width}x${height}`)
    let newWidth = width
    let newHeight = height

    if (width % TILE_SIZE !== 0) {
      newWidth = Math.ceil(width / TILE_SIZE) * TILE_SIZE
      console.log(`Extending width to ${newWidth} (+${newWidth - width}px)`)
    }
    if (height % TILE_SIZE !== 0) {
      newHeight = Math.ceil(height / TILE_SIZE) * TILE_SIZE
      console.log(`Extending height to ${newHeight} (+${newHeight - height}px)`)
    }

    console.log('Tiling...')
    await image
      // FIXME: tiles become offset by 15px in each direction. change gravity?
      .resize({
        width: newWidth,
        height: newHeight,
        background: { r: 0, g: 0, b: 0, alpha: 0 },
      })
      .toFormat(sharp.format.png)
      // FIXME: area beyond image is white, not transparent
      .tile({
        layout: 'google',
        depth: 'onetile',
      })
      .toFile(`./tiles/${floor}`)
  }
}
loop()
