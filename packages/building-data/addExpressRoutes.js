const path = require('path')

const buildings = path.join(__dirname, 'buildings')
const routes = {
  // support all subfolder of the buildings path
  '/data': buildings,
  // also support nicer aliases for some specific subfolders
  '/data/engineering': path.join(buildings, 'ecs-elw-eow'),
}

const makeRelative = path =>
  '/packages/building-data' + path.substr(__dirname.length)

const addBuildingRoutes = (app, express) => {
  for (let [route, dir] of Object.entries(routes)) {
    console.log(`Linking ${route}/**/*(.png|.json) to ${makeRelative(dir)}/*`)
    app.use(route, function (req, res, next) {
      // filter to only allow PNG/JSON
      if (['.png', '.json'].some(ext => req.url.endsWith(ext))) {
        return next()
      }
      // note: the error message hints that it's not reaching express.static
      return next(404)
    }, express.static(dir))
  }
}

module.exports = addBuildingRoutes
