# Data

Basemaps, polygons, and space details for a building or set of buildings. Named
by building code. There are also merged entries like _ecs-elw-eow_. Here are all
the building codes: https://www.uvic.ca/home/about/campus-info/maps/index.php

This could use symlinks to connect _ecs_, _elw_, _eow_ separately to a folder
like _engineering_, but I think it's rare enough to skip for now.

I'm starting only with a few building. There are 136 buildings on campus and
most won't be mapped since they're off topic. I'm focusing on classes and labs
right now.

For example, I have building data for residences, campus security, cafeterias,
gyms, stadiums, server facilities, huts, sheds, etc. These won't be useful to
many people.

Let me know if you have a usecase for them or create an issue/merge request as
needed. I'd like to have all the land (trees and trails) on campus be mapped but
that's not in the scope right now.
