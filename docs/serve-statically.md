You don't need to have a server if you don't want to. It's only responsible for
two things (for now):

- Rewriting URLs, so /data/engineering/{polygons,details,tiles} points to
  ../../building-data/buildings/ecs-elw-eow/{polygons,details,tiles}

- Authenticating people, saving their changes, and emailing me

Currently there aren't plans to support "local edits". If it's not a kiosk then
they should be able to sign in, and then cards will have an edit button. When
they send in changes the server saves them and emails me.

If you _just_ want a map of the current data, and don't want to run an Express
server, you can either:

- Copy the files to the folder you're deploying from in the right layout

- Build a symlink tree that points to the right files under building-data/

- Edit the client's src/util/requests.js

None of these are recommended though because it means you won't be able to git
pull new changes without conflicts.

TODO: the client _could_ support editing the server path...? As part of an
options modal. I've been meaning to do one anyway.
