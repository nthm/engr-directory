# Search

There are many third party options for client side search. This project won't
have many items to search for, so the performance will be great no matter what
library is chosen. However, it's 2018, and a good time to learn how to use web
workers for future projects, so a preference will be given to those
implementations. Note that for such a small search space, a web worker will
have negligible or even negative performance compared to the UI thread.

Using *js-search* for testing right now.

[*js-search*](https://github.com/bvaughn/js-search) comes up high in the list,
and has a demo page showing how to load the script (which is 17K minified and
has no dependencies). It's straight forward to use and supports being passed
objects with several fields.

Here's the example from the project's readme:

```js
import * as JsSearch from 'js-search';

var theGreatGatsby = {
  isbn: '9781597226769',
  title: 'The Great Gatsby',
  author: {
    name: 'F. Scott Fitzgerald'
  },
  tags: ['book', 'inspirational']
};
var theDaVinciCode = {
  isbn: '0307474275',
  title: 'The DaVinci Code',
  author: {
    name: 'Dan Brown'
  },
  tags: ['book', 'mystery']
};
var angelsAndDemons = {
  isbn: '074349346X',
  title: 'Angels & Demons',
  author: {
    name: 'Dan Brown',
  },
  tags: ['book', 'mystery']
};

var search = new JsSearch.Search('isbn');
search.addIndex('title');
search.addIndex(['author', 'name']);
search.addIndex('tags')

search.addDocuments([theGreatGatsby, theDaVinciCode, angelsAndDemons]);

search.search('The');    // [theGreatGatsby, theDaVinciCode]
search.search('scott');  // [theGreatGatsby]
search.search('dan');    // [angelsAndDemons, theDaVinciCode]
search.search('mystery') // [angelsAndDemons, theDaVinciCode]
```

The data for directory.engr follows a similar structure, with tags too, so the
library would be easy to use...

However, it runs in the UI/main thread and
[*js-worker-search*](https://github.com/bvaughn/js-worker-search) (made by the
same person) is recommended as a more simple, worker-based, alternative that
won't block the UI while running.

The worker version does not have English word stemming or TF-IDF (Term
frequency–inverse document frequency) search - which is good since
directory.engr has no use for those features. Unfortunately it has two
dependencies which must be webpack bundled along with any code that wants to
use its API, since it uses an `import` syntax that must be transpiled to work
in browsers.

Here's how other search libraries are related:

*js-worker-search* is a "lighter" fork of *js-search*, which was started as a
lightweight alternative to *lunr.js*, which aimed to recreate some of *Apache
Solr*, which is built on *Apache Lucene*.

*lunr.js* weights in at 35K minified, has no dependencies, and uses some
linear algebra techniques (i.e vectors and dot products) to perform searches.
It has a nice API which is tempting, but also does not employ web workers.

There's a blog post from someone using *lunr.js* in a worker. It's easy to
implement too. Something to keep in mind.

Both *js-worker-search* and *js-search* are **incredibly verbose** and far
more files than they need to be. Follows a strict object oriented approach.

# Tries (data structure) and indexing

### js-worker-search

Document: *"the long road"* -> Tokens: *"the", "long", "road"* -> For each:
```js
const expandedTokens = [];
for (let i = 0, length = token.length; i < length; ++i) {
  let substring = "";

  for (let j = i; j < length; ++j) {
    substring += token.charAt(j);
    expandedTokens.push(substring);
  }
}
```
Now `expandedTokens` of *"the"* is `[t, h, th, the, he, e]`

For each of those, index it with:
```js
/**
 * Maps the specified token to a uid.
 *
 * @param token Searchable token (e.g. "road")
 * @param uid Identifies a document within the searchable corpus
 */
indexDocument(token, uid) {
  if (!this.tokenToUidMap[token]) {
    this.tokenToUidMap[token] = {};
  }
  this.tokenToUidMap[token][uid] = uid;
}
```

Three example rows of the result, after indexing a few documents:
```js
{
  "t":   { u1: u1, u9: u9 },
  ...
  "he":  { u2: u2 },
  ...
  "ong": { u3: u3, u9: u9 },
  ...
  "ing": { u15: u15 }
}
```
**TODO**: Why not use an array instead of an object? Still unanswered

Search the index: (code has been changed)
```js
/**
 * Finds uids that have been mapped to the set of tokens specified.
 * Only uids that have been mapped to all tokens will be returned.
 *
 * @param tokens Array of searchable tokens (e.g. ["long", "road"])
 * @return Array of uids associated with the set of search tokens
 */
search(tokens) {
  let uidMap = {};
  let firstToken = true;
  tokens.forEach(token => {
    let currentUidMap = this.tokenToUidMap[token] || {};
    // TODO: this isn't great for branching et al...
    if (firstToken) {
      firstToken = false;
      // copy the set of all uids that matched the token
      for (let uid in currentUidMap) {
        uidMap[uid] = currentUidMap[uid];
      }
    } else {
      // for all subsequent tokens, reduce the copied set of uids
      for (let uid in uidMap) {
        if (!currentUidMap[uid]) {
          delete uidMap[uid];
        }
      }
    }
  });
  // TODO: why did the original use for loop pushing uidMap values to an array
  return Object.keys(uidMap); // or .values()
}
```

The author calls this a *trie* but is it? There's no walking of a tree. It's
all being handled beneath JS in the code that implements JS objects...
supposedly hashtables. *Update: They're not perfect hashtables, which is why
Map() is used. See below.*

# Homemade search

Build an index and search using it:
```js
const index = {}

function indexJSON(data) {
  console.time('index')
  for (const room in data) {
    const object = data[room]
    let fields = [room, object.name]
    if (object.contact) {
      fields.push(object.contact)
    }
    for (const tag of object.tags) {
      if (!categories[tag]) {
        categories[tag] = []
      }
      categories[tag].push(object)
      fields.push(tag)
    }
    // lowercase each term, split, flatten array results
    fields = [].concat.apply([], fields.map(x => x.toLowerCase().split(' ')))
    for (const token of fields) {
      for (let i = 0, len = token.length; i < len; ++i) {
        let substring = ""
        for (let j = i; j < len; ++j) {
          substring += token.charAt(j)
          // add every substring to the index
          if (!index[substring]) {
            index[substring] = []
          }
          // point to the room, not the document
          // **UPDATE: BUG** see way below in "Objects vs Arrays vs ..." etc
          index[substring].push(room)
        }
      }
    }
  }
  console.timeEnd('index')
  console.log("documents:", Object.keys(data).length)
  console.log("indexes:", Object.keys(index).length)
}
```
Example run: 24 documents created 5538 indexes and took 40ms to run

```js
function search(query) {
  console.time('search')
  const results = {}
  const tokens = query.toLowerCase().split(' ')

  // initial token determines largest result set
  const documents = index[tokens.shift()]

  // copy for non-destructive deletion
  for (const uid in documents) {
    results[uid] = documents[uid]
  }
  // for all subsequent tokens, reduce the copied set
  for (const token of tokens) {
    const tokenResults = index[token]
    for (let uid in results || {}) {
      if (!tokenResults[uid]) {
        delete results[uid]
      }
    }
  }
  console.timeEnd('search')
  return Object.values(results)
}
```

Alternative is to search all the documents *"raw"* directly from JSON:

```js
function noIndexSearch(query, data) {
  console.time('noIndexSearch')
  const tokens = query.toLowerCase().split(' ')
  const results = []
  for (const token of tokens) {
    for (const room in data) {
      const object = data[room]
      let strings = object.tags
      strings.push(room, object.name)
      if (object.contact) {
        strings.push(object.contact)
      }
      strings = strings.map(x => x.toLowerCase())
      for (const string of strings) {
        if (string.includes(token)) {
          results.push(room)
          break
        }
      }
    }
  }
  console.timeEnd('noIndexSearch')
  return results
}
```

The test file should be somewhere in the repo - it's currently as data.js.

The timings are:

- index: 16ms
- search (using it): 0.062ms
- noIndexSearch: 0.43ms

# Multiple data sources

It was originally thought it would be difficult to differentiate between many
indexed files as there is no unique index/key (room can't be used, see issue).
Adding an autoincrementing index to the JSON would be weird... for ordering
and the extra data being sent over the wire. But when the data is restructured
from:

`{ A: {}, B: {}, C: {} }` into `[ { room: A ... }, { room: B ... }, ... ]`

Then each object gets an index naturally because it's an array. In the search
index this can be paired with the version number of the service worker and
the file for each the data came:

service worker / application version : x (i.e 12 or 1.3.9)

| data.json:        | spaces.json:       | staff.json:        |
| ----------------- | ------------------ | ------------------ |
| { data index 0 }, | { space index 0 }, | { staff index 0 }, |
| { data index 1 }, | { space index 1 }, | { ... },           |
| { data index 2 }, | { ... },           | { ... },           |

The search index would then be: `{ 'th': ['data0', 'staff12' ... ] }`

### No index search:
Either:

- Loop through each file and each document linearly and find the strings
- Concatenate the objects once on load or index an array of strings and then
  search the strings. Basically a low-key index

### Indexed search:
Either:

- Add every string and every possible substring of every document to one
  huge object
- Have different indexes for each file and check them one at a time

Either way, whenever pointing to a document you could do: (assuming index)
```
index for v12 is {
    'th': ['data10', 'data11', 'staff0']
    ...
}
```

# V8 internals

### Reference or value?

Is it really more efficient under the hood to have references to
documents rather than the actual documents? Is the implementation of objects
in JS already doing references?

----

**No!** Here's a few resources on the internals of objects in JS and the reason
why the index will be built using a Map() to connect strings to arrays of
objects - not unique indexes.

### V8 internal from Google:
- https://developers.google.com/web/tools/chrome-devtools/memory-problems/
  memory-101

- https://github.com/v8/v8/wiki/Design%20Elements#fast-property-access

### Detailed answer about timings and sizes between maps and objects:
- https://stackoverflow.com/q/18541940/map-vs-object/37994079#37994079

### Lack of compiler optimizations with Maps():
- https://stackoverflow.com/q/44321324/map-vs-object-random-look-ups/44344962

Paraphrased here:

> The reason objects performed so well in JSPerf compared to the Map is
> because under the hood a JS engine can see that we're using the object like
> an array (consecutive integer keys), so it "assumes" that it's an array and
> can make optimisations based on that. Not so with Map.

However, that doesn't explain how arrays in JS are just objects (associative
arrays) but I'm assuming they mean "assumes it's an array" like a C array.
Objects, unlike primitive types, are always passed by reference. An array of
them is as if it were an array of unique indexes. The work is done already.
These internal documents make the previous string-index based solutions look
scary - imagine all those primitiveleaf nodes being held on the heap! This is
also great news for the GeoJSON layers holding space data. Just
reference them and don't worry about their location in memory.

### Objects vs Arrays vs Sets vs Maps

Why did js-worker-search use an object like `'th': { u1: u1 }` and not `'th':
[ u1 ]` ?

---

It actually makes a lot of sense when you read the code, particularily the
index function, which uses the object as a kind of set. I even marked the
code I wrote (based off js-worker-search) as a bug because I'd have duplicate
entries and their code would not: I was pushing to an array but they wrote to
an object so there were no duplicates!

Also,in the search function (re-copied below) they use `delete` to skip the
hassle of using `.splice` and then appropriately readjusting the for loop to
handle a shrinking array - but really it's the set-like feature of objects.
```js
// copy for non-destructive deletion
  for (const uid in documents) {
    results[uid] = documents[uid]
  }
  // for all subsequent tokens, reduce the copied set
  for (const token of tokens) {
    const tokenResults = index[token]
    for (let uid in results || {}) {
      if (!tokenResults[uid]) {
        delete results[uid]
      }
    }
  }
```

In more recent tests, I've used Set() and Map() to achieve a similar idea. See
data.js for the many variations of index and search functions.

# Accessing and displaying data

The hardest part is the schema. For instance, why have `tags` as a field at
all? The tag symbol, **#**, could just be used in the `info` field. But it's
not, because the schema helps decide where things will go on a search result.

Also, tags aren't always the best option. Consider this:

```js
const keysToIndex = ['room', 'name', 'tags']

// note indexJSON is file, keysToIndex, and function called before each object

indexJSON('data/data.json', keysToIndex, object => {
  // should be an array. add a # to make it unique
  object.tags = object.tags.map(x => '#' + x)
})
indexJSON('data/staff.json', [...keysToIndex, 'contact'], object => {
  // fake a tag here rather than have every entry in staff.json have tags
  object.tags = '#staff'
})
indexJSON('data/spaces.json', ['room', 'dept'], object => {
  // hmm
})
```

While it would be wasteful and tedious to have `tags: ['staff']` in front of
every staff entry, it's not exactly ideal to do things this way either. It's
also not true that using a tag will keep the staff list limited to staff.

It would be easy to add #staff to any random document anywhere and impact the
list...

Regardless, the tag-only method breaks down completely with departments, where
the number of departments is not known until run time, and even if each were
added to the index, there would be no way to show them all as a list.

I basically need a SELECT DISTINCT database query. The solution for this is to
add a certain value, whatever that may be, to a Set() and then hold that
outside the index. Has the added benefit of removing the #staff fake tag.

```js
const categories = {
  // by tag
  'Offices':        index.get('#office'),
  'Labs':           index.get('#lab'),
  'Meeting rooms':  index.get('#meeting-room'),
  'Clubs':          index.get('#club'),

  // as lists (since they're unsearchable - kinda)
  'Staff':          indexList.get('staff'),
  'Departments':    indexList.get('departments')
}
```

After all that, categories finally maps titles to sets of list of objects.

**TODO:** Should all categories be an indexList. That way, searches for #lab
don't return off results. A bit more seperation could be good for everyone not
just staff and departments.

Originally I had:

```js
const index = new Map()
const categories = new Map()
```

But then I realized that index's `#lab` and categories' `labs` could be the
same, so why try andkeep the data in two places. Easier all under one roof.

Might not be so bad to have things seperate, however.

Update: Yeah adding '#' just to guarantee uniqueness is kind of redundant.
That's what the `tags: []` field is for to begin with...

Best to have categories be index seperately from free text search, even if
just to not confuse other people in the future.

Also, a nice search index for free text search is one map. Another's for lists
like tags (ie. 'office' or 'club'), staff (only 'staff') which map to an array
of objects, and dept (only 'dept') which maps to an array of strings like
'MENG', 'EENG', etc. lastly there's the spaces.json...

This will be used by the map on hover so it needs to be /fast/. Could do
a search('ELW 207') but that will return all ELW results, then burn it down
to just those that match 'ELW' AND '207'....but if some other room puts 207
in a field - maybe a contact (which is likely) - then the search will return
multiple objects returned. Useless.

Because the spaces.json is ONLY ever used to map rooms to geoJSON, as sort of
a pseudo table join, then it might be best to do the classic:

`{ a: {}, b: {} }` rather than `[ { r: a }, { r: b } ]`

Yes that's good.

Object lookup might even be faster than using Map() because there's no
function call - but that's splitting hairs.

---

The map loads and needs to reference spaces.json (through the `{ a:{}, b:{} }`
style lookup) to get the dept to know what colour to draw the polygons on load.
When someone hovers or clicks on the polygon then what? How does it know what
object to reference in data.json? Is it search the `[{}, {}]` method? That's
linear! Use a hashmap. Store everything in the directory and look it up on
hover.

Also, the space data is then not indexable by `indexJSON` and needs to be
dealt with seperately... The `indexList.get('spaces')` was going to be a quick
lookup for room names but... it's just a list of names. There are no objects.
So searching "EOW 250" might return everything with "250-" as a phone number
... not great. Instead, this indexList is *supposed* to be "safe" to use. But
it's not the same. If the space data is added first to the Set() then order
should be preserved... that would return them first? *Update: YES*

Everything should point to a space else it can't appear on the map; although
staff and perhaps other things can be searched and will have a result, since
they're tied to a space (or many).

Rename `data.json` to `info.json` and restructure back, as in undo, (yes, I
know) to `{ a: {}, b: {} }` Update `info` field to `desc` or something else.

Restructure `staff.json` to the same thing, but as `{ a: [{}], b: [{}] }`
because there may be multiple staff for one room (like student offices such
as the ESS or Co-op Office) **NOTE**: `info.json` cannot have multiple objects
for a space.

Algorithm:
```js
load [spaces.json, info.json, staff.json] as [spaces, info, staff]
for obj of spaces:
  index obj.room obj.dept -> obj // FIRST ROOM
  set obj.room in indexList.spaces (Map?): `obj.room: obj`
  add obj.dept to indexList.departments (Map?): `obj.dept: [{}, ... obj]`
  if *staff*[obj.room]
    obj.staff = that // link
    for person in that
      // maintain a list of other places to find them:
      add obj.room to person.spaces // WILL BE SECOND ROOM
      // need to use netlink here because it's unique:
      set person.netlink to indexList.staff (Map?): `person.netlink: obj`
      // searches for a person's name return the person BEFORE the space
      index person.name person.contact ... -> person // FIRST PERSON
      index person.name person.contact ... -> obj // SECOND PERSON
  if *info*[obj.room]
    obj.info = that // link
    index that.desc that.tags ... -> obj
```

Notice the order of search results to be displayed.
