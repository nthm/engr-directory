Previous worst case HTML generation:

```js
const box = document.createElement('article')
box.className = 'list-item search'

// similar to the hover card but less information
const { name, room, desc, tags, dept, staff } = entry

// using a different style than the card
box.innerHTML = ''
  + `<h3>${name || room}</h3>`
  + (name ?
    `<h4>${room}</h4>` : '')
  + (desc ?
    `<p>${desc}</p>` : '')
  + (tags ?
    `<p>Tags: ${tags.split(' ').map(t =>
      `<span class='tag'>${t}</span>`).join('')
    }</p>` : '')
  + (dept ?
    `<p>Department: ${dept}</p>` : '')
  + (staff ?
    `<div>`
    + `<p>Staff: ${staff.length}</p>${
      staff.map(person => `<p class='staff'>${person.name}</p>`).join('')
    }</div>` : '')

// TODO: mouseover/mouseout for display card on both search and categories
box.addEventListener('click', () => {
  centerMapOnSpace(entry)
  displayCard(entry)
})
```

Now, using voko:

```js
// similar to the hover card but less information
const { name, room, desc, tags, dept, staff } = entry

v('article.list-item.search', {
  onClick() {
    centerMapOnSpace(entry)
    displayCard(entry)
  },
}, [
  v('h3', name || room),
  name && v('h4', room),
  desc && v('p', desc),
  tags && tags.split(' ').map(t =>
    v('span.tag', t)),
  dept && v('p', dept),
  staff && v('', [
    v('p', `Staff: ${staff.length}`),
    staff.map(person =>
      v('p.staff', person.name)),
  ]),
])
```

The voko repository has more examples of how to use the reviver.
