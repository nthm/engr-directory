# Basemaps

TODO: Update this to *not* focus on engineering buildings

These are images under the polygons. They're derived from images on FMIS, a
public server on campus, but the images uploaded here have been cleaned up,
grayscaled, joined, and otherwise modified.

The individual basemaps must be tiled to be used by Leaflet (kind of, see
_image-overlay.md_ for details). They're exports from GIMP. The file _all.xcf_
contains the fuzzy selected outlines of the floor plans, spread per floor and
per building - but aligned to overlap perfectly. Each GIMP layer is transparent,
except for an all white background to make the lines appear.

For touch ups, pathways were removed along with some text boxes from ECS. The
whitespace surrounding buildings was removed by fuzzy selecting with "Feathered
edges" set to **2px**, then _Select_ > _Shrink_ by **5px**, and then pressing
delete to clear.

It's a waste of data/information to have floors 5-7 show the roofs of ELW and
EOW (since ECS is the only build). But the alternative of simply showing the
previous tile layer doesn't work due to the way tiles load in. This is why: if
someone starts on floor 6, how does the application know to load the 5th floor
tiles? A special HTTP code per tile? Can't just return "204 No Content" because
that might mean it's just transparent. Regardless, it's performing a depth first
search (!!) just to load tiles. It would make a lot more sense if the floors
were loaded as entire images for each building - but obviously that is bad for
other reasons.

For now this works. There might be a way to find all the tiles that only differ
by opacity/colour and then do symlinks or something to save space but... Keep it
simple.
