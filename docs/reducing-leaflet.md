# Reducing Leaflet.js

Leaflet was recently converted to use ESM. They still choose to use their own
implementation of classes, given their targeted browser support (IE 7/8!), and
that their entire codebase is built on it. This is great since it means you can
selectively import only pieces of Leaflet you actually use, and compile/polyfill
it for your own range of browsers. However, as of July 22 2018, Leaflet is still
heavily bundled to itself, and hard to tree shake.

In [#5704](https://github.com/Leaflet/Leaflet/issues/5704), an author mentions:

> [ESM is] ongoing work that is not yet completely finished. The main problem at
> the moment is making sure people can use Leaflet with module imports, but
> still also use Leaflet plugins that wasn't written with modules or any bundler
> in mind. That quickly gets tricky.

In [#5696](https://github.com/Leaflet/Leaflet/issues/5696):

> [...] I want add Popup in my bundle, but I don't need Path. Path module still
> be in the bundle for only one reason – for [`if (!(this._source instanceof
> Path))`] check, and it don't used in Popup module in any other way. The same
> situation with FeatureGroup class. I can remove Popup module by removing one
> line or with tree-shaking, but if I want to remove Path module, I have to
> change code inside other modules.

Which isn't great. Here's the entire Leaflet tree. Try to remember that this
project only needs a slippy map that renders tiles and polygons.

```
/node_modules/leaflet/src/
├── control
│   ├── Control.Attribution.js
│   ├── Control.js
│   ├── Control.Layers.js
│   ├── Control.Scale.js
│   ├── Control.Zoom.js
│   └── index.js
├── core
│   ├── Browser.js
│   ├── Class.js
│   ├── Class.leafdoc
│   ├── Events.js
│   ├── Events.leafdoc
│   ├── Handler.js
│   ├── index.js
│   └── Util.js
├── dom
│   ├── DomEvent.DoubleTap.js
│   ├── DomEvent.js
│   ├── DomEvent.Pointer.js
│   ├── DomUtil.js
│   ├── Draggable.js
│   ├── index.js
│   └── PosAnimation.js
├── geo
│   ├── crs
│   │   ├── CRS.Earth.js
│   │   ├── CRS.EPSG3395.js
│   │   ├── CRS.EPSG3857.js
│   │   ├── CRS.EPSG4326.js
│   │   ├── CRS.js
│   │   ├── CRS.Simple.js
│   │   └── index.js
│   ├── index.js
│   ├── LatLngBounds.js
│   ├── LatLng.js
│   └── projection
│       ├── index.js
│       ├── Projection.LonLat.js
│       ├── Projection.Mercator.js
│       └── Projection.SphericalMercator.js
├── geometry
│   ├── Bounds.js
│   ├── index.js
│   ├── LineUtil.js
│   ├── Point.js
│   ├── PolyUtil.js
│   └── Transformation.js
├── images
│   ├── layers.svg
│   ├── logo.svg
│   └── marker.svg
├── layer
│   ├── DivOverlay.js
│   ├── FeatureGroup.js
│   ├── GeoJSON.js
│   ├── ImageOverlay.js
│   ├── index.js
│   ├── LayerGroup.js
│   ├── Layer.Interactive.leafdoc
│   ├── Layer.js
│   ├── marker
│   │   ├── DivIcon.js
│   │   ├── Icon.Default.js
│   │   ├── Icon.js
│   │   ├── index.js
│   │   ├── Marker.Drag.js
│   │   └── Marker.js
│   ├── Popup.js
│   ├── tile
│   │   ├── GridLayer.js
│   │   ├── index.js
│   │   ├── TileLayer.js
│   │   └── TileLayer.WMS.js
│   ├── Tooltip.js
│   ├── vector
│   │   ├── Canvas.js
│   │   ├── Circle.js
│   │   ├── CircleMarker.js
│   │   ├── index.js
│   │   ├── Path.js
│   │   ├── Polygon.js
│   │   ├── Polyline.js
│   │   ├── Rectangle.js
│   │   ├── Renderer.getRenderer.js
│   │   ├── Renderer.js
│   │   ├── SVG.js
│   │   ├── SVG.Util.js
│   │   └── SVG.VML.js
│   └── VideoOverlay.js
├── Leaflet.js
└── map
    ├── handler
    │   ├── Map.BoxZoom.js
    │   ├── Map.DoubleClickZoom.js
    │   ├── Map.Drag.js
    │   ├── Map.Keyboard.js
    │   ├── Map.ScrollWheelZoom.js
    │   ├── Map.Tap.js
    │   └── Map.TouchZoom.js
    ├── index.js
    ├── Map.js
    └── Map.methodOptions.leafdoc

14 directories, 89 files
```

No need for CRS or projections, DOM utilities (I use voko), image and video
overlays, vectors beyond just polygons, GeoJSON support, popups, and lastly
markers. I assume everything in the Leaflet core will be brought along always
which is fine.

---

Originally I imported Leaflet as `L` by linking to an already bundled build of
all of Leaflet: `/dist/leaflet-src.js` which is specified in their package.json

Reducing the Leaflet bundle. Before it was imported as their singleton which
weighed: Stat: 150.9kb Parsed: 136.8kb GZip: 39.2kb

![](https://gitlab.com/nthm/engr-directory/uploads/71b12d88623b07799f4c3f5c4552a96b/2018.07.27..17.55.20.png)

After selecting only the necessary components (and doing some tricks to avoid
importing certain chains) it weighed: Stat: 273.9kb (?) Parsed: 87.4kb GZip:
24.8kb

![](https://gitlab.com/nthm/engr-directory/uploads/c464d43b47aafca94004fc0d5b27d3c5/2018.07.27..17.55.26.png)

With concatenateModules: true then the overall bundle size (all application
code) goes from 106kb to 96kb, but then the individual per-module stats are
unknown since it's concatenated.

However, the overall gzip change is from 39kb to 32kb.

See _map/index.js_ for how its now imported in a more modular way.
