# Project layout

Here's an explanation of the folders and files in the project under `packages/`:

## Building data

Basemaps, polygons, and space details for a building or set of buildings. See
the readme.md for details. Contains scripts of scraping from FMIS.

## Client

```
.
├── build/                  Output from webpack compilation. Don't read this
│   ├── index.html          Run this in your browser
│   └── ...
├── res/
│   ├── icons/              UI SVGs
│   └── images/             UI PNGs (mostly for the sidebar, UVic logo, etc)
├── src/                    Source of the application. Read this
│   ├── card/               Hover card
│   │   ├── style.css
│   │   └── index
│   ├── directory/          Index of all data, and a search function
│   │   └── index
│   ├── map/                The map, and all Leaflet code
│   │   ├── polygons        Anything polygon related
│   │   │   ├── load
│   │   │   └── style
│   │   ├── buttons         Buttons along the left next to the sidebar
│   │   ├── elevator        Change floors (replace basemaps and cache layers)
│   │   ├── restoreView     Saves lat/lng, zoom, level to LocalStorage
│   │   ├── toSpace         Fly to a space
│   │   └── index
│   ├── modal/              Modals, their background shade, and content
│   │   ├── index
│   │   └── style.css
│   ├── sidebar/            Sidebar
│   │   ├── categories      Primary view, renders dropdowns from the directory
│   │   │   └── index
│   │   ├── keyboard        Onscreen keyboard for the kiosk (when searching)
│   │   │   ├── index
│   │   │   └── style.css
│   │   ├── search          Swaps with categories to search the directory
│   │   │   └── index
│   │   ├── index
│   │   └── style.css
│   ├── util/               Shared code
│   │   └── requests        Requests files in res/ (polygons & directory JSON)
│   ├── index.ejs           The template HTML used by HTMLWebpackPlugin
│   ├── index               Entry point. Start reading here
│   ├── style.css
│   └── sw                  Service worker. Cache everything for offline use
├── package.json
├── package-lock.json
├── readme.md
└── webpack.config.js
```

### Naming (import/exports) of `client/`

Generally start reading at the `index.js` files of directory. For import and
exports, _Capitalized_ names are for objects or groups that are crossing file
boundaries; only the top-most import/export should be capitalized, see examples.
Using _camelCase_ names for functions and everything else.

Avoiding the need to mentally expand `*`, so everything is explicitly imported.
Exports are generally at _end_ of a file, rather than throughout.

## Server

Simple Express app for now. Will update documentation as server develops.
