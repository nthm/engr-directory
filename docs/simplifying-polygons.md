The original polygon payloads used absolute integer coordinates to describe
spaces. Together, they weighed in at 112kB (24kB GZip). After getting used to
TopoJSON at work, I wrote a script to convert the polygons to relative
coordinates to help reduce the size of the payload.

I also noticed some lines had redundant points, such as [(10,0), (8,0), (5,0)]
instead of simply [(10,0), (5,0)], so removed those. The script can be found at
backend/processPolygons.js

Here's an example of the old polygons:
```json
{
  "ECS 366": [[1280,895],[999,895],[999,1247],[1280,1247],[1280,895]],
  "ECS 360": [[1280,1252],[999,1252],[999,1579],[1280,1579],[1280,1252]],
  "ECS 354": [[1280,1584],[999,1584],[999,1919],[1280,1919],[1280,1584]],
  "ECS 348": [[1280,1924],[999,1924],[999,2259],[1280,2259],[1280,1924]],
  "ECS 342": [[1280,2264],[999,2264],[999,2599],[1280,2599],[1280,2264]],
  ...
```

New:
```json
{
  "ECS 366": [[1280,895],[-281,0],[0,352],[281,0],[0,-352]],
  "ECS 360": [[1280,1252],[-281,0],[0,327],[281,0],[0,-327]],
  "ECS 354": [[1280,1584],[-281,0],[0,335],[281,0],[0,-335]],
  "ECS 348": [[1280,1924],[-281,0],[0,335],[281,0],[0,-335]],
  "ECS 342": [[1280,2264],[-281,0],[0,335],[281,0],[0,-335]],
  ...
```

The algorithm compares the slopes of line segments, so it removes redundant
points in diagonal lines as well.

Some output:
```
4.json:
...
ECS 422    -10 bytes.
ECS 424    -10 bytes.
ECS 463    -28 bytes.
ELW 401    -40 bytes. Redundant points removed: 2
ELW 430   -261 bytes. Redundant points removed: 18
ELW 420   -297 bytes. Redundant points removed: 21
...
Delta for file: -8619 bytes

5.json:
...
ECS 526    -10 bytes.
ECS 528    -10 bytes.
ECS 530    -10 bytes.
Delta for file: -946 bytes

Total delta: -28336 bytes
```

28 kB saved!

Running `tar -c res/polygons | gzip -9 - | wc -c` on each directory shows the
new polygon come in at 16kB. Comapred to the original 24kB.

The hyperscript reviver will fit nicely in that free space, though I expect the
codebase to shrink after it replaces the verbose DOM APIs currently in use.

The code to unpack the relative coordinates trivial, and has no runtime impact.
See commit a74a05.

Also, this is what it looks like to apply that code to the old polygons:

![](./images/skew-polygons.png)
