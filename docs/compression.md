# GZip, Tar, JSON, and efficiency

A lot of time has went to testing different JSON formats to understand when
there is room for improvement - in both file size and transfer times. You can
see the GitLab issue's for details, but it mostly boiled down to deciding
whether or not to use geoJSON. Internally, Leaflet takes geoJSON and converts
it to its own data types, which build off of one another. The original plan
was to use its "raw" Polygon type and just pass arrays of numbers in JSON.

The issue on GitLab was closed because geoJSON was favour for its
interoperability with programs like QGIS and OpenJMP.

## Sizes:

geoJSONs with polygons, and space details. one file for each building and
each floor:
```sh
> for file in polygons/*/*.geojson
    jq --compact-output . $file > $file.min
    gzip -9 -c $file > $file.gz
    gzip -9 -c $file.min > $file.min.gz
  end

> du -h --total polygons/*/*.geojson
240K
> du -h --total polygons/*/*.geojson.gz
72K

> du -h --total polygons/*/*.geojson.min
224K
> du -h --total polygons/*/*.geojson.min.gz
72K
```
so it's negligible to minify before gz, and makes more sense to maintain
newlines and spaces for readability when the browser decompresses the file.

note however, that using tar to bundle all geoJSONs and *then* gzipping it
is only **32KB** (!!) down from 240KB (!!)

```sh
> tar -c -v (find polygons/ -name '*.new') | gzip -9 -c - > bundle.tar.gz
> du -h bundle.tar.gz
32KB
```

```js
// playground
require('isomorphic-fetch')
const fs = require('fs')

// generated with `find polygons -name '*.new'`
const paths = [
    'polygons/0/EOW.geo.json.new',
    'polygons/1/ECS.geo.json.new',
    'polygons/1/EOW.geo.json.new',
    'polygons/1/ELW.geo.json.new',
    'polygons/2/ECS.geo.json.new',
    'polygons/2/EOW.geo.json.new',
    'polygons/2/ELW.geo.json.new',
    'polygons/3/ECS.geo.json.new',
    'polygons/3/EOW.geo.json.new',
    'polygons/3/ELW.geo.json.new',
    'polygons/4/ECS.geo.json.new',
    'polygons/4/EOW.geo.json.new',
    'polygons/4/ELW.geo.json.new',
    'polygons/5/ECS.geo.json.new',
    'polygons/6/ECS.geo.json.new'
]

const floors = new Array(7)
console.log(floors)
const fetches = []
for (const path of paths) {
    const req = fetch(`http://localhost:8000/${path}`)
        .then(res => res.json())
        .then(data => {
            const building = path.match(/(E[A-Z]{2})/g)
            const floor = parseInt(path.match(/[0-9]/g))
            console.log(floor)
            const newData = floors[floor] || {}
            for (const entry of data.features) {
                const key = building + ' ' + entry.properties.room
                const value = entry.geometry.coordinates
                newData[key] = value
            }
            floors[floor] = newData
        })
    fetches.push(req)
}
Promise.all(fetches).then(()=> {
    for (const floor in floors) {
        const obj = floors[floor]
        fs.writeFile(`/tmp/${floor}.json`, JSON.stringify(obj), err => {
            if (err) {
                console.error(err)
            }
        })
        console.log('rooms:', Object.keys(obj).length)
    }
})
```

Produces files `[0..6].json` where each is a floor merging all buildings in
the format:

```json
{
    "EOW 231": [[x1,y1],[x2,y2],[x3,y3] ... ],
    "ECS 550": [[x1,y1],[x2,y2],[x3,y3] ... ] ...
}
```

Which **must** be paired with `spaces.json` as previously mentioned in
`search.md`. Together...

```sh
> du -h --total /tmp/*.json
4.0K    /tmp/0.json
24K     /tmp/1.json
24K     /tmp/2.json
28K     /tmp/3.json
12K     /tmp/4.json
8.0K    /tmp/5.json
8.0K    /tmp/6.json
108K    total

> for file in /tmp/*.json
      gzip -9 -c $file > $file.gz
  end

> du -h --total /tmp/*.json.gz
4.0K    /tmp/0.json.gz
8.0K    /tmp/1.json.gz
8.0K    /tmp/2.json.gz
8.0K    /tmp/3.json.gz
4.0K    /tmp/4.json.gz
4.0K    /tmp/5.json.gz
4.0K    /tmp/6.json.gz
40K     total
```

Let's try the bundle:

```sh
> tar -c -v (seq -f 'data/%g.json' 0 6) | gzip -9 -c - > bundle.tar.gz
> du -h bundle.tar.gz
23KB
```

Which, when paired with the `spaces.json` that's also gzipped it comes out to
*23KB + 5KB = 28KB* vs the pure geoJSON version of *32KB*.

However, consider network requests. The "smaller" version is 7 requests. The
main payload on the initial load, and then one for each floor. Meanwhile, the
geoJSON could be *ONE* request if it's merged without tar - and it's
interoperable.

Continuing to move forward with the geoJSON since it's working. Need to test a
merged geoJSON either per floor or for the whole application.

On another note: Used *zopflipng* with "more iterations" (`-m` flag) on the
map tiles. 3743 tiles were reduced from 20MB to 18MB and it took 4 hours...

Worth a shot.

Passing the folder through tar brings it down to 7.9MB. There's no compression
applied, it's just the way the filesystem works:

https://stackoverflow.com/q/497024/tar-files-smaller-than-their-contents

> You get a difference because of the way the filesystem works.
>
> In a nutshell your disk is made out of clusters. Each cluster has a fixed
> size of - let's say - 4 kilobytes. If you store a 1kb file in such a cluster
> 3kb will be unused. The exact details vary with the kind of file-system that
> you use, but most file-systems work that way.
>
> 3kb wasted space is not much for a single file, but if you have lots of very
> small files the waste can become a significant part of the disk usage.
>
> Inside the tar-archive the files are not stored in clusters but one after
> another. That's where the difference comes from.

Uploaded the tar.gz which is 4.2MB

## Download the bundle for the client

There's a beautiful project called *js-untar* that is still actively
maintained (a month ago!) at https://github.com/InvokIT/js-untar

The library does exactly as it sounds and is only **6.5KB** minified with no
dependencies!

It would be worth it then, to offer the option to download all tiles and JSON
to the client, saving them extra requests and data - keep in mind that gz
works best on larger files, as shown above.

This was brought to my attention after seeing a similar idea with zip files:
https://serviceworke.rs/cache-from-zip.html where they use zip.js to populate
a service worker cache.

The only caveat so far is that on the initial application load, the first
floor and its JSON has already been loaded by the client since the map starts
zoomed out at level 2 (from 0 to 5), so all those tiles are requested.

It seems I set the `{min,max}Zoom` in `map.js` to 2 and 5 respectively. Which
means the bundle should not ship zoom levels 0 and 1.

It's 3681 tiles (6.3MB) for skipping zoom levels 0 and 1, and 3674 if we skip
zoom level 2 of floor 1... the change in size is less than 0.1MB so it's not
worth it.

The new two-border-trans/ files are nice. Zopfli compressed them to 88% the size
of their GIMP exports on average.
